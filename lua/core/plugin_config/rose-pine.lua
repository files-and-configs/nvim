vim.o.termguicolors = true

require('rose-pine').setup({
    disable_background = true,
})

vim.cmd [[ colorscheme rose-pine ]]
