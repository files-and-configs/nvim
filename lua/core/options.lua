require('core/utils')

Glb.mapleader = ','
Glb.c_syntax_for_h = true

Opt.number = true
Opt.relativenumber = true
Opt.cursorline = true
Opt.laststatus = 0
Opt.swapfile = false
Opt.shiftwidth = 4
Opt.expandtab = true
Opt.clipboard = 'unnamedplus'
Opt.lazyredraw = true
Opt.pumheight = 8
Opt.splitbelow = true
Opt.splitright = true
Opt.path = '.,/usr/include'
Opt.mousemodel = 'extend'
Opt.wrap = false
Opt.wildignore = '*.docx,*.jpg,*.png,*.gif,*.pdf,*.exe,*.o,*.mp3,*.mp4,*.mov,*.mkv,*.ogg,*.dat,*.obj'
Opt.scrolloff = 5
Opt.sidescrolloff = 5
Opt.foldmethod = 'marker'
Opt.timeoutlen = 500
Opt.termguicolors = true
